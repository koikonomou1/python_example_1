import pytest

@pytest.fixture

def client():
	from experiments import app
	app.app.config['Test'] = True
	return app.app.test_client()