from urllib.parse import urlencode
import json

def call(client, path, params):
    url = path + '?' + urlencode(params)
    response = client.get(url)
    return json.loads(response.data.decode('utf-8'))

def test_add_one(client):
	result = call(client,'/add_one', {'x': 4})
	assert result['x'] == 5
	# assert result.json['x'] == result

def test_square(client):
	result = call(client, '/square', {'x': 3})
	assert result['x'] == 9