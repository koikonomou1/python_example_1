#!/usr/bin/env python
from flask import request, Flask
import json
from functools import wraps

app = Flask(__name__)

@app.route('/add_one')

def add_one():
    x = int(request.args.get('x', 1))
    out = json.dumps({'x': x + 1})
    with open('workfile', 'w') as f:
        f.write(out)
    return json.dumps({'x': x + 1})

@app.route('/square')

def square():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x * x})


@app.route('/add_one_from_json', methods=['POST'])

def add_one_from_json():
    data = request.get_json(force=True)
    try:
        value = int(data['x'])
    except (KeyError, TypeError, ValueError):
        raise JsonError(description='Invalid value.')
    return json.dumps({'x': value + 1})

@app.route('/square_json', methods=['POST'])

def square_json():
    data = request.get_json(force=True)
    try:
        value = int(data['x'])
    except (KeyError, TypeError, ValueError):
        raise JsonError(description='Invalid value.')
    return json.dumps({'x': value * value})