from app import app
from flask import json
import sys


def test_add_one_from_json():
	with open('values.json') as json_file:
		value = json.load(json_file)
	response = app.test_client().post('/add_one_from_json', data=json.dumps(value), content_type='application/json', )
	data = json.loads(response.get_data(as_text=True))
	print ('add_one_from_json =', data['x'])
	with open('test_add_one_from_json.txt', 'w') as f:
		f.write('%d \n' % data['x'])
	assert response.status_code == 200
	assert data['x'] == 8

def test_square_json():
	with open('values.json') as json_file:
		value = json.load(json_file)
	response = app.test_client().post('/square_json', data=json.dumps(value), content_type='application/json', )
	data = json.loads(response.get_data(as_text=True))
	print ('square_from_json =', data['x'])
	with open('test_square_json.txt', 'w') as f:
		f.write('%d \n' % data['x'])
	assert response.status_code == 200
	assert data['x'] == 49
