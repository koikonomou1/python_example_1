An example repository that adds in selected numbers the number one and also gives square numbers.

## How it works 
git clone https://gitlab.com/koikonomou1/python_example_1/ \
cd python_example_1 \
sudo docker-compose -f docker-compose.yml up --build \
sudo docker build -t calculations \
sudo docker run --publish 9061:9061 -it calculations

### Example 1 
Add in x = 3 the number 1 \
curl http://0.0.0.0:9061/add_one?x=3

output {"x": 4}

### Example 2
Μultiplies the number by itself \
curl http://0.0.0.0:9061/square?x=3

output {"x": 9}

## POST json file with curl
curl -X POST -H "Content-Type: application/json" -d @ FILENAME DESTINATION
